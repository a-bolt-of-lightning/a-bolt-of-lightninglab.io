---
layout: page
title: About
permalink: /about/
---
## Greetings! 
I'm called Vladimir!
My main interests are software development and AI, I also do a bit of back-end coding with popular frameworks and in my free-time I write funny automation apps with python, read fantasy novels that have Elves and play Elder Scrolls games.


_Note: I'm not a fan of fancy front-end development, and that's why I chose jekyll: to setup my page quickly and neatly._


##### [My LinkedIn Page](https://www.linkedin.com/in/fmahvari/)
##### [My Github Page](https://github.com/a-bolt-of-lightning)
