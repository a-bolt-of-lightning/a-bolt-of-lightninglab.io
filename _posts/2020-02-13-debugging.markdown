---
layout: post
title:  "My Favourite Debugging Method: Rubber Duck Debugging!"
date:   2020-02-13 14:51:33 +0330
categories: jekyll update
---

My favorite debugging method, although I don't use a rubber duck, I prefer talking to my laptop or my notebook if it's around, they look more intelligent than ducks and also they don't have eyes so they won't be looking at you mockingly when you explain a fairly simple problem to them, which makes them better listeners.

I just found out that the whole concept is from the book "The Pragmatic Programmer" written by Andrew Hunt and David Thomas, and I'm planning on reading the newest edition of it :)

[link to the article](https://blog.codinghorror.com/rubber-duck-problem-solving/)